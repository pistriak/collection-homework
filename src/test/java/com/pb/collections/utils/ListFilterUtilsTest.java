package com.pb.collections.utils;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ListFilterUtilsTest {

    @Test
    void filter() {
        List<Integer> ints = List.of(1, 2, 3);

        List<Integer> filteredInts = ListFilterUtils.filter(ints, e -> e % 2 == 0);

        assertEquals(List.of(2), filteredInts);
    }
}